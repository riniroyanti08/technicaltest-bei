<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Page_Monitor</name>
   <tag></tag>
   <elementGuidId>d6cc7f19-3a4a-4c7d-a07e-ff66eccfa704</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#tbodyid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='tbodyid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#tbodyid</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7867874e-cd71-40f1-9cd0-f484f92ff7c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>a16612b1-ef15-4bb5-b2f9-f03963f75881</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tbodyid</value>
      <webElementGuid>6b818de4-243a-483f-be4d-e4433def6a59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apple monitor 24$400LED Cinema Display features a 27-inch glossy LED-backlit TFT 
active-matrix LCD display with IPS technology and an optimum resolution 
of 2560x1440. It has a 178 degree horizontal and vertical viewing angle,
 a &quot;typical&quot; brightness of 375 cd/m2, contrast ratio of 1000:1, and a 
12 ms response time.  ASUS Full HD$230ASUS VS247H-P 23.6- Inch Full HD  </value>
      <webElementGuid>03fce250-7a9d-47fe-b82b-800df4dd3928</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tbodyid&quot;)</value>
      <webElementGuid>70a2d42c-e531-4fa4-b5bb-0d632c1a63b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='tbodyid']</value>
      <webElementGuid>44367888-6c40-4400-bc74-661e1b902b56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contcont']/div/div[2]/div</value>
      <webElementGuid>7ec441dc-d734-415b-a40a-c686ff31c132</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CATEGORIES'])[1]/following::div[2]</value>
      <webElementGuid>025d45ee-617e-4023-8ecb-ca4fd7eb934a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::div[6]</value>
      <webElementGuid>2811f102-9a27-4b3e-905c-e401b9317841</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[2]/div</value>
      <webElementGuid>9e85b0ee-15fa-422f-b065-d66461d007b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'tbodyid' and (text() = 'Apple monitor 24$400LED Cinema Display features a 27-inch glossy LED-backlit TFT 
active-matrix LCD display with IPS technology and an optimum resolution 
of 2560x1440. It has a 178 degree horizontal and vertical viewing angle,
 a &quot;typical&quot; brightness of 375 cd/m2, contrast ratio of 1000:1, and a 
12 ms response time.  ASUS Full HD$230ASUS VS247H-P 23.6- Inch Full HD  ' or . = 'Apple monitor 24$400LED Cinema Display features a 27-inch glossy LED-backlit TFT 
active-matrix LCD display with IPS technology and an optimum resolution 
of 2560x1440. It has a 178 degree horizontal and vertical viewing angle,
 a &quot;typical&quot; brightness of 375 cd/m2, contrast ratio of 1000:1, and a 
12 ms response time.  ASUS Full HD$230ASUS VS247H-P 23.6- Inch Full HD  ')]</value>
      <webElementGuid>bb9ff2ce-ae4d-41fc-b7dd-42a428df52da</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
