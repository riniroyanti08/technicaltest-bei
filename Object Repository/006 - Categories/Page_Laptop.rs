<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Page_Laptop</name>
   <tag></tag>
   <elementGuidId>43f72c4e-52b3-4053-be4d-ec87a4acef14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#tbodyid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='tbodyid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#tbodyid</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>db8a7b40-3190-488e-9468-fa80cd9cdbf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>52e47f65-06dc-46c6-8b6f-53e6a3e54797</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tbodyid</value>
      <webElementGuid>94ee7c59-31ad-43df-a5bb-9419292db5f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sony vaio i5$790Sony is so confident that the VAIO S is a superior ultraportable laptop 
that the company proudly compares the notebook to Apple's 13-inch 
MacBook Pro. And in a lot of ways this notebook is better, thanks to a 
lighter weight.  Sony vaio i7
$790REVIEW
 
Sony is so confident that the VAIO S is a superior 
ultraportable laptop that the company proudly compares the notebook to 
Apple's 13-inch MacBook Pro. And in a lot of ways this notebook is 
better, thanks to a lighter weight, higher-resolution display, more 
storage space, and a Blu-ray drive.   MacBook air$7001.6GHz dual-core Intel Core i5 (Turbo Boost up to 2.7GHz) with 3MB 
shared L3 cache
Configurable to 2.2GHz dual-core Intel Core i7 (Turbo 
Boost up to 3.2GHz) with 4MB shared L3 cache.  Dell i7 8gb$7006th Generation Intel Core i7-6500U Dual-Core Processor 2.5 GHz (max 
boost speed up to 3.1GHz) 4MB L3 Cache, 8GB DDR4 1600 MHz, 1TB 5400 RPM 
HDD15.6 in Full HD LED-backlit touchscreen with Truelife (1920 x 1080), 
10-finger multi-touch support, Intel HD Graphics 520 with shared 
graphics memory  2017 Dell 15.6 Inch$7007th Gen Intel Core i7-7500U mobile processor 2.70 GHz with Turbo Boost 
Technology up to 3.50 GHz, Intel HD Graphics 62015.6 inch Full HD IPS 
TrueLife LED-backlit touchscreen (1920 x 1080), 10-finger multi-touch 
support, 360° flip-and-fold design,8GB DDR4 2400 MHz Memory, 1TB 5400 
RPM HDD, No optical drive, 3 in 1 card reader (SD SDHC SDXC)  MacBook Pro$1100Apple has introduced three new versions of its MacBook Pro line, 
including a 13-inch and 15-inch model with the Touch Bar, a thin, 
multi-touch strip display that sits above the MacBook Pro's keyboard.   </value>
      <webElementGuid>bcc0401d-e904-4154-a464-f3cfefc96fd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tbodyid&quot;)</value>
      <webElementGuid>bed553dc-5a16-4a53-9ca2-78b360bf5940</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='tbodyid']</value>
      <webElementGuid>48d57d3d-6c81-4c42-a1c3-6855ded6dda1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contcont']/div/div[2]/div</value>
      <webElementGuid>32285f8e-6106-4ec1-8c99-1dc521b8de8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CATEGORIES'])[1]/following::div[2]</value>
      <webElementGuid>210a9940-9706-46ab-a329-da3b8dd777f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::div[6]</value>
      <webElementGuid>10c51831-d585-48d0-bef1-3ac3c7b1a2fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[2]/div</value>
      <webElementGuid>adc192c5-c7c7-4a1f-a01e-57791671ea2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'tbodyid' and (text() = concat(&quot;Sony vaio i5$790Sony is so confident that the VAIO S is a superior ultraportable laptop 
that the company proudly compares the notebook to Apple&quot; , &quot;'&quot; , &quot;s 13-inch 
MacBook Pro. And in a lot of ways this notebook is better, thanks to a 
lighter weight.  Sony vaio i7
$790REVIEW
 
Sony is so confident that the VAIO S is a superior 
ultraportable laptop that the company proudly compares the notebook to 
Apple&quot; , &quot;'&quot; , &quot;s 13-inch MacBook Pro. And in a lot of ways this notebook is 
better, thanks to a lighter weight, higher-resolution display, more 
storage space, and a Blu-ray drive.   MacBook air$7001.6GHz dual-core Intel Core i5 (Turbo Boost up to 2.7GHz) with 3MB 
shared L3 cache
Configurable to 2.2GHz dual-core Intel Core i7 (Turbo 
Boost up to 3.2GHz) with 4MB shared L3 cache.  Dell i7 8gb$7006th Generation Intel Core i7-6500U Dual-Core Processor 2.5 GHz (max 
boost speed up to 3.1GHz) 4MB L3 Cache, 8GB DDR4 1600 MHz, 1TB 5400 RPM 
HDD15.6 in Full HD LED-backlit touchscreen with Truelife (1920 x 1080), 
10-finger multi-touch support, Intel HD Graphics 520 with shared 
graphics memory  2017 Dell 15.6 Inch$7007th Gen Intel Core i7-7500U mobile processor 2.70 GHz with Turbo Boost 
Technology up to 3.50 GHz, Intel HD Graphics 62015.6 inch Full HD IPS 
TrueLife LED-backlit touchscreen (1920 x 1080), 10-finger multi-touch 
support, 360° flip-and-fold design,8GB DDR4 2400 MHz Memory, 1TB 5400 
RPM HDD, No optical drive, 3 in 1 card reader (SD SDHC SDXC)  MacBook Pro$1100Apple has introduced three new versions of its MacBook Pro line, 
including a 13-inch and 15-inch model with the Touch Bar, a thin, 
multi-touch strip display that sits above the MacBook Pro&quot; , &quot;'&quot; , &quot;s keyboard.   &quot;) or . = concat(&quot;Sony vaio i5$790Sony is so confident that the VAIO S is a superior ultraportable laptop 
that the company proudly compares the notebook to Apple&quot; , &quot;'&quot; , &quot;s 13-inch 
MacBook Pro. And in a lot of ways this notebook is better, thanks to a 
lighter weight.  Sony vaio i7
$790REVIEW
 
Sony is so confident that the VAIO S is a superior 
ultraportable laptop that the company proudly compares the notebook to 
Apple&quot; , &quot;'&quot; , &quot;s 13-inch MacBook Pro. And in a lot of ways this notebook is 
better, thanks to a lighter weight, higher-resolution display, more 
storage space, and a Blu-ray drive.   MacBook air$7001.6GHz dual-core Intel Core i5 (Turbo Boost up to 2.7GHz) with 3MB 
shared L3 cache
Configurable to 2.2GHz dual-core Intel Core i7 (Turbo 
Boost up to 3.2GHz) with 4MB shared L3 cache.  Dell i7 8gb$7006th Generation Intel Core i7-6500U Dual-Core Processor 2.5 GHz (max 
boost speed up to 3.1GHz) 4MB L3 Cache, 8GB DDR4 1600 MHz, 1TB 5400 RPM 
HDD15.6 in Full HD LED-backlit touchscreen with Truelife (1920 x 1080), 
10-finger multi-touch support, Intel HD Graphics 520 with shared 
graphics memory  2017 Dell 15.6 Inch$7007th Gen Intel Core i7-7500U mobile processor 2.70 GHz with Turbo Boost 
Technology up to 3.50 GHz, Intel HD Graphics 62015.6 inch Full HD IPS 
TrueLife LED-backlit touchscreen (1920 x 1080), 10-finger multi-touch 
support, 360° flip-and-fold design,8GB DDR4 2400 MHz Memory, 1TB 5400 
RPM HDD, No optical drive, 3 in 1 card reader (SD SDHC SDXC)  MacBook Pro$1100Apple has introduced three new versions of its MacBook Pro line, 
including a 13-inch and 15-inch model with the Touch Bar, a thin, 
multi-touch strip display that sits above the MacBook Pro&quot; , &quot;'&quot; , &quot;s keyboard.   &quot;))]</value>
      <webElementGuid>cdca4f27-550c-4fbb-8d5d-9a7582c64ce2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
