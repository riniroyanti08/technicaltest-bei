<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Page_phone</name>
   <tag></tag>
   <elementGuidId>86cf2e0f-49dd-43be-947a-5267d1e94b65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#tbodyid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='tbodyid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#tbodyid</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c9e667d3-a92e-4a7f-a4d1-e27ada155545</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>35681e1d-4dd1-4979-a13c-55f78c675a00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tbodyid</value>
      <webElementGuid>220e1334-25fe-4d3a-b3a2-bd1a634962ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Samsung galaxy s6$360The Samsung Galaxy S6 is powered by 1.5GHz octa-core Samsung Exynos 7420
 processor and it comes with 3GB of RAM. The phone packs 32GB of 
internal storage cannot be expanded.   Nokia lumia 1520$820The Nokia Lumia 1520 is powered by 2.2GHz quad-core Qualcomm Snapdragon 800 processor and it comes with 2GB of RAM.   Nexus 6$650The Motorola Google Nexus 6 is powered by 2.7GHz quad-core Qualcomm Snapdragon 805 processor and it comes with 3GB of RAM.  Samsung galaxy s7$800The Samsung Galaxy S7 is powered by 1.6GHz octa-core it comes with 4GB 
of RAM. The phone packs 32GB of internal storage that can be expanded up
 to 200GB via a microSD card.  Iphone 6 32gb$790It comes with 1GB of RAM. The phone packs 16GB of internal storage 
cannot be expanded. As far as the cameras are concerned, the Apple 
iPhone 6 packs a 8-megapixel primary camera on the rear and a 
1.2-megapixel front shooter for selfies.  Sony xperia z5$320Sony Xperia Z5 Dual smartphone was launched in September 2015. The phone
 comes with a 5.20-inch touchscreen display with a resolution of 1080 
pixels by 1920 pixels at a PPI of 424 pixels per inch.  HTC One M9$700The HTC One M9 is powered by 1.5GHz octa-core Qualcomm Snapdragon 810 
processor and it comes with 3GB of RAM. The phone packs 32GB of internal
 storage that can be expanded up to 128GB via a microSD card.   </value>
      <webElementGuid>11d88150-c96d-4656-baa1-a0cda60a7146</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tbodyid&quot;)</value>
      <webElementGuid>33fd2738-5ffe-4058-8e93-7bbb3bb861e9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='tbodyid']</value>
      <webElementGuid>d638386e-3b1f-4cfa-9262-71ff4b1091c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contcont']/div/div[2]/div</value>
      <webElementGuid>59f05846-2fe0-4aca-86d1-ecb03b4d810a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CATEGORIES'])[1]/following::div[2]</value>
      <webElementGuid>d07c9c2e-4f48-4a9d-8c83-c4411933dfd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::div[6]</value>
      <webElementGuid>43db2a5b-05b6-4bf5-9a6e-85e12913e6b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[2]/div</value>
      <webElementGuid>0b5ce2aa-1a2f-47b9-bd0a-363da1e510a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'tbodyid' and (text() = 'Samsung galaxy s6$360The Samsung Galaxy S6 is powered by 1.5GHz octa-core Samsung Exynos 7420
 processor and it comes with 3GB of RAM. The phone packs 32GB of 
internal storage cannot be expanded.   Nokia lumia 1520$820The Nokia Lumia 1520 is powered by 2.2GHz quad-core Qualcomm Snapdragon 800 processor and it comes with 2GB of RAM.   Nexus 6$650The Motorola Google Nexus 6 is powered by 2.7GHz quad-core Qualcomm Snapdragon 805 processor and it comes with 3GB of RAM.  Samsung galaxy s7$800The Samsung Galaxy S7 is powered by 1.6GHz octa-core it comes with 4GB 
of RAM. The phone packs 32GB of internal storage that can be expanded up
 to 200GB via a microSD card.  Iphone 6 32gb$790It comes with 1GB of RAM. The phone packs 16GB of internal storage 
cannot be expanded. As far as the cameras are concerned, the Apple 
iPhone 6 packs a 8-megapixel primary camera on the rear and a 
1.2-megapixel front shooter for selfies.  Sony xperia z5$320Sony Xperia Z5 Dual smartphone was launched in September 2015. The phone
 comes with a 5.20-inch touchscreen display with a resolution of 1080 
pixels by 1920 pixels at a PPI of 424 pixels per inch.  HTC One M9$700The HTC One M9 is powered by 1.5GHz octa-core Qualcomm Snapdragon 810 
processor and it comes with 3GB of RAM. The phone packs 32GB of internal
 storage that can be expanded up to 128GB via a microSD card.   ' or . = 'Samsung galaxy s6$360The Samsung Galaxy S6 is powered by 1.5GHz octa-core Samsung Exynos 7420
 processor and it comes with 3GB of RAM. The phone packs 32GB of 
internal storage cannot be expanded.   Nokia lumia 1520$820The Nokia Lumia 1520 is powered by 2.2GHz quad-core Qualcomm Snapdragon 800 processor and it comes with 2GB of RAM.   Nexus 6$650The Motorola Google Nexus 6 is powered by 2.7GHz quad-core Qualcomm Snapdragon 805 processor and it comes with 3GB of RAM.  Samsung galaxy s7$800The Samsung Galaxy S7 is powered by 1.6GHz octa-core it comes with 4GB 
of RAM. The phone packs 32GB of internal storage that can be expanded up
 to 200GB via a microSD card.  Iphone 6 32gb$790It comes with 1GB of RAM. The phone packs 16GB of internal storage 
cannot be expanded. As far as the cameras are concerned, the Apple 
iPhone 6 packs a 8-megapixel primary camera on the rear and a 
1.2-megapixel front shooter for selfies.  Sony xperia z5$320Sony Xperia Z5 Dual smartphone was launched in September 2015. The phone
 comes with a 5.20-inch touchscreen display with a resolution of 1080 
pixels by 1920 pixels at a PPI of 424 pixels per inch.  HTC One M9$700The HTC One M9 is powered by 1.5GHz octa-core Qualcomm Snapdragon 810 
processor and it comes with 3GB of RAM. The phone packs 32GB of internal
 storage that can be expanded up to 128GB via a microSD card.   ')]</value>
      <webElementGuid>de9e6ca8-9d52-4333-90f0-997010b9ad42</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
