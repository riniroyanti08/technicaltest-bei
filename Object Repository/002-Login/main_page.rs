<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>main_page</name>
   <tag></tag>
   <elementGuidId>c14ae61a-b698-4c13-90c9-16bedfe35016</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#narvbarx</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='narvbarx']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;PRODUCT STORE Home (current) Contact About us Cart Log in Log out Welcome testak&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>nav</value>
      <webElementGuid>4efa6459-511e-40ff-ad14-fbd0d51dfbdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>narvbarx</value>
      <webElementGuid>681aab88-1040-439c-8f35-fe6dd779e8ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar navbar-toggleable-md bg-inverse</value>
      <webElementGuid>944d7672-965e-4522-a60c-846fb6983796</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
      
    
    
      PRODUCT STORE
    
      
        
          Home (current)
        
        
          Contact
        
        
          About us
        
        
          Cart
        
        
          Log in
        
        
          Log out
        
        
          Welcome testakun
        
        
          Sign up
        
      
    
    
      
        
          
          
          
        
        
          
            
          
          
            
          
          
            
          
        
        
          
          Previous
        
        
          
          Next
        
      
    
  </value>
      <webElementGuid>14e215b6-5547-4ab1-b819-083621a73bbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;narvbarx&quot;)</value>
      <webElementGuid>48a3b132-aae1-48d9-ab9d-87c0bf3edd75</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//nav[@id='narvbarx']</value>
      <webElementGuid>92ff794e-c2e5-4fa4-b673-109e27d9bf8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[4]/following::nav[1]</value>
      <webElementGuid>f0a3c988-b1a3-4ac9-aece-0785bc9cda87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close Modal Dialog'])[1]/following::nav[1]</value>
      <webElementGuid>2a5bdee9-25aa-4cbf-85d7-69a76a107ec1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav</value>
      <webElementGuid>329bb6e7-b5cf-44b9-aeeb-1b7c7c30a599</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//nav[@id = 'narvbarx' and (text() = '
    
      
    
    
      PRODUCT STORE
    
      
        
          Home (current)
        
        
          Contact
        
        
          About us
        
        
          Cart
        
        
          Log in
        
        
          Log out
        
        
          Welcome testakun
        
        
          Sign up
        
      
    
    
      
        
          
          
          
        
        
          
            
          
          
            
          
          
            
          
        
        
          
          Previous
        
        
          
          Next
        
      
    
  ' or . = '
    
      
    
    
      PRODUCT STORE
    
      
        
          Home (current)
        
        
          Contact
        
        
          About us
        
        
          Cart
        
        
          Log in
        
        
          Log out
        
        
          Welcome testakun
        
        
          Sign up
        
      
    
    
      
        
          
          
          
        
        
          
            
          
          
            
          
          
            
          
        
        
          
          Previous
        
        
          
          Next
        
      
    
  ')]</value>
      <webElementGuid>a4ffec4f-b601-4d70-b9d6-588bb8956001</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
