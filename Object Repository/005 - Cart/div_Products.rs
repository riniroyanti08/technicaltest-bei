<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Products</name>
   <tag></tag>
   <elementGuidId>e45d18ae-8208-4673-b5db-42b21ddeb45f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-8</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Products Pic Title Price x Samsung galaxy s6360Delete&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7cbaa647-7a51-41db-8d90-a3816350789c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-8</value>
      <webElementGuid>202e7f08-7b93-41a3-942f-d8c0e237bc3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Products
        
          
            
              
                Pic
                Title
                Price
                x
              
            
            
            Samsung galaxy s6360Delete
          
        
      </value>
      <webElementGuid>abfa3af1-442b-4d43-b79f-f95b5f11ed64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-8&quot;]</value>
      <webElementGuid>b1615cff-e695-4b42-835d-236b39de4cd8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div</value>
      <webElementGuid>85bbefa2-5f53-4cfb-a9a7-e9516f5d6011</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(current)'])[1]/following::div[3]</value>
      <webElementGuid>272d60e4-6292-46e0-81bd-4663905b8184</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div</value>
      <webElementGuid>9847c1b1-c752-4dc6-b96d-746d9efa6090</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Products
        
          
            
              
                Pic
                Title
                Price
                x
              
            
            
            Samsung galaxy s6360Delete
          
        
      ' or . = '
        Products
        
          
            
              
                Pic
                Title
                Price
                x
              
            
            
            Samsung galaxy s6360Delete
          
        
      ')]</value>
      <webElementGuid>27ff0173-c849-4c4e-84af-7bac827cb62f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
